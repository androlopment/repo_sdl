#ifndef SAMPLECLASS_H
#define SAMPLECLASS_H


class SampleClass
{
    public:
        SampleClass();
        virtual ~SampleClass();
        SampleClass(const SampleClass& other);
        unsigned int GetCounter() { return m_Counter; }
        void SetCounter(unsigned int val) { m_Counter = val; }
        bool GetRunning() { return m_Running; }
        void SetRunning(bool val) { m_Running = val; }
    protected:
    private:
        unsigned int m_Counter;
        bool m_Running;
};

#endif // SAMPLECLASS_H
