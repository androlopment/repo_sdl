#ifndef CSURFACE_H
#define CSURFACE_H


class CSurface
{
    public:
        CSurface();
        virtual ~CSurface();
        static SDL_Surface* OnLoad(char* File);
        static bool OnDraw(SDL_Surface* Surf_Dest, SDL_Surface* Surf_Src, int X, int Y);
    protected:
    private:
};

#endif // CSURFACE_H
