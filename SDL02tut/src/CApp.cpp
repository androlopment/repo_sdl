#include "CApp.h"

/** \brief main constructor
 *
 * \param
 * \param
 * \return
 *
 */

CApp::CApp()
{
    //ctor
    Running = true;

    Surf_Display = NULL;
    Surf_Test = NULL;
}

/** \brief destructor
 *
 * \param
 * \param
 * \return
 *
 */

CApp::~CApp()
{
    //dtor
}

/** \brief main loop of the app where all the main triggers are managed
 *
 * \param
 * \param
 * \return
 *
 */

int CApp::OnExecute()
{
    if(OnInit()==false)
        return -1;

    SDL_Event Event;

    while(Running){

        while (SDL_PollEvent(&Event)){
            OnEvent(&Event);
        }

        OnLoop();

        OnRender();

    }

    OnCleanup();

    return 0;
}

/** \brief main entry point of the SDL test app
 *
 * \param
 * \param
 * \return
 *
 */

int main(int argc, char* argv[]){
    CApp theApp;

    return theApp.OnExecute();
}

/** \brief
 *
 * \param
 * \param
 * \return
 *
 */
bool CApp::OnInit(){

    if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        return false;
    }

    if((Surf_Display = SDL_SetVideoMode(640, 480, 32, SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL) {
        return false;
    }

    return true;
}

void CApp::OnEvent(SDL_Event* Event){
    if(Event->type == SDL_QUIT) {
            Running = false;
    }
}

void CApp::OnLoop(){

}

void CApp::OnRender(){

}

void CApp::OnCleanup(){
    SDL_FreeSurface(Surf_Display);
    SDL_FreeSurface(Surf_Test);
    SDL_Quit();

}

